function [curve] = FitCurve(thresholdedSignal)
    %% Convert 2D signal to a weighted points
    xVal = [];
    yVal = [];
    wVal = [];
    for x=1:size(thresholdedSignal, 2)
       for y=1:size(thresholdedSignal, 1)
            if(thresholdedSignal(y, x) > 0)
               xVal = [xVal, x];
               yVal = [yVal, y];
               wVal = [wVal, thresholdedSignal(y, x)^2];
            end
        end 
    end
    
    %% Fit a smoothing spline to the weighted points. 
    % This makes most sense, since we know the car won't have a discontinuous speed
    curve = createFit(xVal, yVal, wVal);
end

function [fitresult, gof] = createFit(xVal, yVal, wVal)
    % Prepare data for curve fitting
    [xData, yData, weights] = prepareCurveData(xVal, yVal, wVal);

    % Set up fittype and options.
    ft = fittype('smoothingspline');
    opts = fitoptions('Method', 'SmoothingSpline');
    opts.Normalize = 'on';
    opts.SmoothingParam = 0.999998838198419;    % Based on a good fit to the training data
    opts.Weights = weights;

    % Fit model to data.
    [fitresult, gof] = fit( xData, yData, ft, opts );
end
