function [result] = FilterAndCombine(singleStepCorrelation, doubleStepCorrelation)
    %% Single step
    singleStepCorrelation = flattenAndRemoveBlockSplittingArtifacts(singleStepCorrelation);
    
    % Gauss and median filtering to smooth and remove noise outliers
    singleStepCorrelation = imgaussfilt(singleStepCorrelation, [1, 3]);
    singleStepCorrelation = medfilt2(singleStepCorrelation, [3, 10]);
    
    %% Double step
    doubleStepCorrelation = flattenAndRemoveBlockSplittingArtifacts(doubleStepCorrelation);
    
    % Gauss and median filtering to smooth and remove noise outliers
    doubleStepCorrelation = imgaussfilt(doubleStepCorrelation, [2, 3]);
    doubleStepCorrelation = medfilt2(doubleStepCorrelation, [6, 10]);

    %% Combine results to further reduce noise and improve accuracy
    result = singleStepCorrelation + imresize(doubleStepCorrelation, [size(singleStepCorrelation, 1), size(singleStepCorrelation, 2)]);
end

function [result] = flattenAndRemoveBlockSplittingArtifacts(correlation)
    result = correlation;
    for i=1:size(result, 1)
       % Flatten the correlation image
       result(i, :) = (i^0.75) * result(i, :);
       
       % Remove block splitting lines by averaging the line below and above
       if(mod(i-1, 8) == 0 && i > 2)
           result(i, :) = (result(i-1, :) + result(i+1, :));
       end
    end
end
