function [] = ConvertToBirdsEyeVideo(inputPath, outputPath)
    %% Constants
    FRAME_RATE = 20;

    FOCAL_LENGTH = [250 250];       % Based on popular smartphone camera parameters found online. Tweaked until conversion result looked right.
    PRINCIPAL_POINT = [309 222];    % "Measured" by taking the convergent point of the lane markers (which are parallel lines)
    IMAGE_SIZE = [480 640];
    HEIGHT = 1.5;                   % Guess for the height of the camera sensor off the ground. Tweaked until conversion result looked right.
    PITCH = 0;
    
    %% Code
    camIntrinsics = cameraIntrinsics(FOCAL_LENGTH, PRINCIPAL_POINT, IMAGE_SIZE);
    sensor = monoCamera(camIntrinsics, HEIGHT, 'Pitch', PITCH);
    
    inputVideo = VideoReader(inputPath);
    birdsEyeVideo = VideoWriter(outputPath);
    birdsEyeVideo.open();
    i = 1;
    while(inputVideo.hasFrame())
       frame = inputVideo.readFrame();
       birdsEyeVideo.writeVideo(getBirdsEyeFrame(frame, sensor));
       if(mod(i, 100) == 0)
          fprintf("Converted frame %d / %d\n", i, FRAME_RATE * inputVideo.Duration);
       end
       i = i + 1;
    end
    birdsEyeVideo.close();
end

function [result] = getBirdsEyeFrame(frame, sensor)
    %% Constants
    DIST_AHEAD = 6;                 % These constants set the "window" for the bird's eye view conversion.
    SPACE_TO_SIDE = 4;              % They are chosen to be significantly larger than the max speed frame shift,
    BOTTOM_OFFSET = 3;              % but small enough that cars in front are rarely in view.
    
    OUT_IMAGE_SIZE = [NaN, 250];    % Automatically set height based on a 250 wide output
    
    %% Convert frame
    outView = [BOTTOM_OFFSET, DIST_AHEAD, -SPACE_TO_SIDE, SPACE_TO_SIDE];
    birdsEye = birdsEyeView(sensor, outView, OUT_IMAGE_SIZE);
    result = birdsEye.transformImage(frame);
end