# Comma.ai Speed Challenge
## My solution

My solution to the [Comma.ai Speed challenge](https://github.com/commaai/speedchallenge) is based on basic Computer Vision algorithms.
These are the general steps:  
  
1. Convert video to bird's eye view  
2. Calculate correlation between a single and double frame step  
3. Find correlation peak in longitudinal direction per frame  
4. Filter these correlation peaks  
5. Fit a continuous spline to the weighted correlation data  

## Results
![Results](https://bitbucket.org/DebboR/comma.ai-speed-challenge/raw/c2a0e71e112063baac5e9595ff7fa52416afca2c/Output/Results.png =500x)
A higher resolution image is available in the "Output" folder.  
  
**MSE with respect to training data:** 0.41  
**MSE with respect to test data:** 3.7
