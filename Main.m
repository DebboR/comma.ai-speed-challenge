clc;
close all;
clear all;

%% Constants
trainVideoPath = 'train.mp4';
testVideoPath = 'test.mp4';
trainDataPath = 'train.txt';
testDataPath = 'test.txt';

trainVideoBirdsEyePath = 'train_BE';
testVideoBirdsEyePath = 'test_BE';

%% Convert training and test videos to bird's eye view
disp("Converting training video to bird's eye view");
ConvertToBirdsEyeVideo(trainVideoPath, trainVideoBirdsEyePath);

disp("Converting test video to bird's eye view");
ConvertToBirdsEyeVideo(testVideoPath, testVideoBirdsEyePath);

disp("Done converting input videos");
%% Process bird's eye videos to find correlation between the frames
disp("Calculating correlation of train video");
[trainSingleStepCorrelation, trainDoubleStepCorrelation] = CalculateCorrelation(trainVideoBirdsEyePath);

disp("Calculating correlation of test video");
[testSingleStepCorrelation, testDoubleStepCorrelation] = CalculateCorrelation(testVideoBirdsEyePath);

disp("Done calculating correlations");

%% Filter and combine resulting correlation signals
combinedTrainSignal = FilterAndCombine(trainSingleStepCorrelation, trainDoubleStepCorrelation);
combinedTestSignal = FilterAndCombine(testSingleStepCorrelation, testDoubleStepCorrelation);

%% Threshold signals
thresholdedTrainSignal = combinedTrainSignal .* (combinedTrainSignal > 0.4);
thresholdedTestSignal = combinedTestSignal .* (combinedTestSignal > 0.4);
disp("Done filtering, combining and thresholding correlation signals");

%% Fit curve to thresholded signals
trainFit = FitCurve(thresholdedTrainSignal);
testFit = FitCurve(thresholdedTestSignal);

%% Remove 1 due to Matlab indexing, and multiply by constant to best fit to the training data
estimatedTrainSpeed = (trainFit(1:size(thresholdedTrainSignal, 2))-1).*1.07;
estimatedTestSpeed = (testFit(1:size(thresholdedTestSignal, 2))-1).*1.07;
disp("Done fitting curves to the signals");

%% Save test fit to test.txt
csvwrite(testDataPath, estimatedTestSpeed);
disp("Wrote test output");

%% Plot results
% Plot training data
subplot(4, 2, 1);
h = surf(imresize(trainSingleStepCorrelation, [400, 2000]));
set(h, 'EdgeColor', 'none');
view(2);
title("Single Frame Step Train Correlation");

subplot(4, 2, 3);
h = surf(imresize(trainDoubleStepCorrelation, [400, 2000]));
set(h, 'EdgeColor', 'none');
view(2);
title("Double Frame Step Train Correlation");

subplot(4, 2, 5);
h = surf(imresize(thresholdedTrainSignal, [400, 2000]));
set(h, 'EdgeColor', 'none');
view(2);
title("Thresholded Train Correlation");

subplot(4, 2, 7);
plot(1:size(thresholdedTrainSignal, 2), estimatedTrainSpeed);
hold on;
trainSpeed = csvread("train.txt");
plot(trainSpeed);
title(sprintf("MSE: %f", immse(trainSpeed, estimatedTrainSpeed)));
hold off;
axis([0, size(thresholdedTrainSignal, 2), -5, 40]);
legend("Estimated speed", "Actual speed");

% Plot test data
subplot(4, 2, 2);
h = surf(imresize(testSingleStepCorrelation, [400, 2000]));
set(h, 'EdgeColor', 'none');
view(2);
title("Single Frame Step Test Correlation");

subplot(4, 2, 4);
h = surf(imresize(testDoubleStepCorrelation, [400, 2000]));
set(h, 'EdgeColor', 'none');
view(2);
title("Double Frame Step Test Correlation");

subplot(4, 2, 6);
h = surf(imresize(thresholdedTestSignal, [400, 2000]));
set(h, 'EdgeColor', 'none');
view(2);
title("Thresholded Test Correlation");

subplot(4, 2, 8);
plot(1:size(thresholdedTestSignal, 2), estimatedTestSpeed);
axis([0, size(thresholdedTestSignal, 2), -5, 40]);
title("Estimated speed");
