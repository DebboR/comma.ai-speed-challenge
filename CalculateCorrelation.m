function [singleStepCorrelation, doubleStepCorrelation] = CalculateCorrelation(inputVideoPath)  
    FRAME_RATE = 30;

    inputVideo = VideoReader(inputVideoPath + ".avi");
    prevPrevFrame = inputVideo.read(1);
    prevFrame = inputVideo.read(1);
    frame = inputVideo.read(1);
    
    inputVideo = VideoReader(inputVideoPath + ".avi");

    singleStepCorrelation = [];
    doubleStepCorrelation = [];

    i = 1;
    while(inputVideo.hasFrame())
       prevPrevFrame(:, :, :) = prevFrame(:, :, :);
       prevFrame(:, :, :) = frame(:, :, :); 
       frame = inputVideo.readFrame();

       singleStepCorrelation = [singleStepCorrelation, getYCorrelation(prevFrame, frame, 40).'];
       doubleStepCorrelation = [doubleStepCorrelation, getYCorrelation(prevPrevFrame, frame, 80).'];

       if(mod(i, 100) == 0)
          fprintf("Calculated frame %d / %d\n", i, round(FRAME_RATE * inputVideo.Duration));
       end

       i = i + 1;
    end
end

function [yCorrelation] = getYCorrelation(movingFrame, fixedFrame, correlationRange)
    % Convert from uint8 to single data type for FFT performance
    movingFrame = single(movingFrame);
    fixedFrame = single(fixedFrame);
    
    % Add windowing to the image to decrease FFT leakage
    blackmanWindow = createBlackmanWindow(size(movingFrame));
    movingFrame = movingFrame .* blackmanWindow;
    fixedFrame  = fixedFrame .* blackmanWindow;
    
    % Compute phase correlation matrix, D
    D = phasecorr(fixedFrame, movingFrame);
    
    % Take the mean between R, G and B channel to get a single channel correlation result
    yCorrelation = mean(D, 3);
    
    % Reduce 2D -> 1D in the movement direction. We don't care about horizontal movement.
    yCorrelation = max(yCorrelation(1:correlationRange, 1:50).');
end

%--------------------------------------------
% Functions by MathWorks
%--------------------------------------------
function h = createBlackmanWindow(windowSize)
    % Define Blackman window to reduce finite image replication effects in
    % frequency domain. Blackman window is recommended in (Stone, Tao,
    % McGuire, Analysis of image registration noise due to rotationally
    % dependent aliasing).

    M = windowSize(1);
    N = windowSize(2);

    a0 = 7938/18608;
    a1 = 9240/18608;
    a2 = 1430/18608;

    n = 1:N;
    m = 1:M;

    % Make outer product degenerate if M or N is equal to 1.
    h1 = 1;
    h2 = 1;
    if M > 1
        h1 = a0 - a1*cos(2*pi*m / (M-1)) + a2*cos(4*pi*m / (M-1));
    end
    if N > 1
        h2 = a0 - a1*cos(2*pi*n / (N-1)) + a2*cos(4*pi*n / (N-1));
    end

    h = h1' * h2;
end

function d = phasecorr(A,B)
    %PHASECORR Compute phase correlation matrix
    %
    %   D = PHASECORR(A, B) computes the phase correlation matrix from input
    %   2-D images A and B. PHASECORR returns the 'full' correlation matrix
    %   similar to normxcorr2.

    %   Copyright 2013 The MathWorks, Inc.

    size_A  = size(A);
    size_B  = size(B);

    % Let fft2 zero pad time domain signals such that we form the 'full'
    % correlation matrix analogous to the result produced by normxcorr2.
    outSize = size_A + size_B - 1;

    % Form 2-D spectra of moving and fixed.
    % Window each signal prior to taking FFT to help reduce ringing effects in
    % frequency domain due to finite image.
    A = fft2(A, outSize(1), outSize(2));
    B = fft2(B, outSize(1), outSize(2));

    % Form phase correlation matrix, d
    % Use 'symmetric' option as performance optimization. We expect that the
    % input moving images A and B are real valued, so d should always be real
    % valued.
    ABConj = A .* conj(B);
    d = ifft2(ABConj ./ abs(eps+ABConj), 'symmetric');
end

